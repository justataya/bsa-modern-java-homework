package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem = null;

	private DefenciveSubsystem defenciveSubsystem = null;

	private int maxShieldHP;

	private int maxHullHP;

	private int maxCapacitorAmount;

	private boolean attacked = false;

	public static CombatReadyShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		var ship = new CombatReadyShip();
		ship.name = name;
		ship.shieldHP = shieldHP;
		ship.hullHP = hullHP;
		ship.powergridOutput = powergridOutput;
		ship.capacitorAmount = capacitorAmount;
		ship.capacitorRechargeRate = capacitorRechargeRate;
		ship.speed = speed;
		ship.size = size;

		ship.maxHullHP = hullHP.value();
		ship.maxShieldHP = shieldHP.value();
		ship.maxCapacitorAmount = capacitorAmount.value();

		ship.attackSubsystem = attackSubsystem;
		ship.defenciveSubsystem = defenciveSubsystem;

		return ship;
	}

	@Override
	public void endTurn() {
		int capacitor = Math.min(this.maxCapacitorAmount,
				this.capacitorAmount.value() + this.capacitorRechargeRate.value());
		this.capacitorAmount = new PositiveInteger(capacitor);
		this.attacked = false;
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.attacked || this.attackSubsystem.getCapacitorConsumption().value() > this.capacitorAmount.value()) {
			return Optional.empty();
		}

		this.attacked = true;
		this.capacitorAmount = new PositiveInteger(
				this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());

		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction resultDamage = this.defenciveSubsystem.reduceDamage(attack);

		int resultShieldHP = this.shieldHP.value() - resultDamage.damage.value();
		int resultHullHP = this.hullHP.value();

		if (resultShieldHP < 0) {
			resultHullHP += resultShieldHP;
			resultShieldHP = 0;
		}

		if (resultHullHP <= 0) {
			this.hullHP = new PositiveInteger(0);
			this.shieldHP = new PositiveInteger(0);
			return new AttackResult.Destroyed();
		}

		this.hullHP = new PositiveInteger(resultHullHP);
		this.shieldHP = new PositiveInteger(resultShieldHP);

		return new AttackResult.DamageRecived(attack.weapon, resultDamage.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		if (this.defenciveSubsystem.getCapacitorConsumption().value() > this.capacitorAmount.value()) {
			return Optional.empty();
		}

		int regenerateHullHP = Math.min(this.maxHullHP,
				this.hullHP.value() + regenerateAction.hullHPRegenerated.value()) - this.hullHP.value();
		int regenerateShieldHP = Math.min(this.maxShieldHP,
				this.shieldHP.value() + regenerateAction.shieldHPRegenerated.value()) - this.shieldHP.value();
		RegenerateAction resultRegenerateAction = new RegenerateAction(new PositiveInteger(regenerateShieldHP),
				new PositiveInteger(regenerateHullHP));

		this.capacitorAmount = new PositiveInteger(
				this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		this.hullHP = new PositiveInteger(this.hullHP.value() + regenerateHullHP);

		return Optional.of(resultRegenerateAction);
	}

}
