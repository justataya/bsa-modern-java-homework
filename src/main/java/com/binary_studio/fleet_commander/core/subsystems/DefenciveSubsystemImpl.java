package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.isEmpty() || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		DefenciveSubsystemImpl subsystem = new DefenciveSubsystemImpl();
		subsystem.name = name;
		subsystem.powergridConsumption = powergridConsumption;
		subsystem.capacitorConsumption = capacitorConsumption;
		subsystem.impactReductionPercent = impactReductionPercent.value() > 95 ? new PositiveInteger(95)
				: impactReductionPercent;
		subsystem.shieldRegeneration = shieldRegeneration;
		subsystem.hullRegeneration = hullRegeneration;

		return subsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		return new AttackAction(
				new PositiveInteger((int) Math.ceil(incomingDamage.damage.value()
						- this.impactReductionPercent.value() / 100.0 * incomingDamage.damage.value())),
				incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
