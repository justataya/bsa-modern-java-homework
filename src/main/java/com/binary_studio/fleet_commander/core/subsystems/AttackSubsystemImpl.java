package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isEmpty() || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		AttackSubsystemImpl subsystem = new AttackSubsystemImpl();
		subsystem.name = name;
		subsystem.powergridRequirments = powergridRequirments;
		subsystem.capacitorConsumption = capacitorConsumption;
		subsystem.optimalSpeed = optimalSpeed;
		subsystem.optimalSize = optimalSize;
		subsystem.baseDamage = baseDamage;

		return subsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
				: (double) target.getSize().value() / this.optimalSize.value();
		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		return new PositiveInteger(
				(int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
