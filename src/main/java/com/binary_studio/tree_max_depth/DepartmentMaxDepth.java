package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		Queue<Department> queue = new LinkedList<>();

		queue.add(rootDepartment);
		queue.add(null);

		int depth = 1;
		Department current;
		while (!queue.isEmpty()) {
			current = queue.remove();
			if (current == null && !queue.isEmpty()) {
				depth++;
				queue.add(null);
			}
			else if (current != null) {
				for (Department department : current.subDepartments) {
					if (department != null) {
						queue.add(department);
					}
				}
			}
		}

		return depth;
	}

}
