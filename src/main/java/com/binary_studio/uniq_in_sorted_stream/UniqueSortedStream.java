package com.binary_studio.uniq_in_sorted_stream;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		AtomicReference<Object> atomicReference = new AtomicReference<>();
		return t -> {
			var key = keyExtractor.apply(t);
			if (!key.equals(atomicReference.get())) {
				atomicReference.set(key);
				return true;
			}
			return false;
		};
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(distinctByKey(Row::getPrimaryId));
	}

}
